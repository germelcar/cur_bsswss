#ifdef __cplusplus
extern "C" {
#endif

#include <R.h>
#include <Rinternals.h>
#include <R_ext/Rdynload.h>


extern void cuda_bsswss(const char* ds_source, int ds_rows, int ds_cols,
		double* ds_output, int* c1, int c1_cols, int* c2,
		int c2_cols, int nloop);


SEXP bsswss(SEXP ds_source, SEXP rows_dssource, SEXP cols_dssource,
		SEXP dsc1, SEXP dim_dsc1, SEXP dsc2, SEXP dim_dsc2, SEXP nloop){


	SEXP ds_output;
	R_len_t total_protected = 0;
	int* _dsc1;
	int* _dsc2;
	double* _ds_output;

	/*
	 * 0 ==> ROWS
	 * 1 ==> COLS
	 */
	R_len_t _nloop = INTEGER(nloop)[0];
	R_len_t _rows = INTEGER(rows_dssource)[0];
	R_len_t _cols = INTEGER(cols_dssource)[0];
	R_len_t dsc1_cols = INTEGER(dim_dsc1)[1];
	R_len_t dsc2_cols = INTEGER(dim_dsc2)[1];

	/* Avoiding labels column (for Leukemia it will be 7129 columns) */
	_cols--;
	R_len_t n_ds_output = _cols * _nloop;

	_dsc1 = INTEGER(dsc1);
	_dsc2 = INTEGER(dsc2);

	/* Allocating memory for final output vector */
	PROTECT(ds_output = allocVector(REALSXP, n_ds_output));
	total_protected++;
	_ds_output = REAL(ds_output);

	/* Running CUDA external function */
	cuda_bsswss(CHAR(STRING_ELT(ds_source, 0)), _rows, _cols, _ds_output,
			_dsc1, dsc1_cols, _dsc2, dsc2_cols, _nloop);


	UNPROTECT(total_protected);

	return ds_output;
}




R_CallMethodDef callMethods[] = {
   {"bsswss", (DL_FUNC)&bsswss, 8},
   {NULL, NULL, 0}
 };

void R_init_myLib (DllInfo *info) {
R_registerRoutines (info, NULL, callMethods,
				   NULL, NULL);
}


#ifdef __cplusplus
} // closing brace for extern "C"
#endif
