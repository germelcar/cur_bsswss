#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>

#define MAX_THREADS_PER_BLOCK 512

double* read_ds_and_fill_array(const char* ds_filename,
		int ds_rows, int ds_cols){

	double* array;
	FILE* fp;
	array = (double*) malloc(sizeof(double) * ds_rows * ds_cols);

	if(array == NULL){
		printf("%s %s at line %d. %s.\n",
				"Error in", __FILE__, __LINE__,
				"No enough memory");
		return NULL;
	}

	if ((fp = fopen(ds_filename, "rt")) == NULL){
		printf("%s %s at line %d. %s %s.\n",
				"Error in", __FILE__, __LINE__,
				"Error reading file", ds_filename);
		return NULL;
	}

	unsigned long long index = 0;
	for(int i = 0; i < ds_rows; i++){
		fscanf(fp, "%*d%*c"); /* Avoiding the label */
		for(int j = 0; j < ds_cols; j++)
			fscanf(fp, "%*d:%lf%*c", &array[index++]);

	}

	fclose(fp);
	return array;
}

__global__ void kernel_bsswss(double* data, int* c1, int* c2, double* bsswss,
		int nloop, int ROWS, int COLS, int c1_length, int c2_length,
		int c1c2_length){


	int tid = blockDim.x * blockIdx.x + threadIdx.x;

	/* if tid is greater or equal to COLS, then, go out.
	 * Otherwise, do some taks.
	 */
	if(tid >= COLS)
		return;


		double Xkj_c1			= 0.0;
		double Xkj_c2			= 0.0;
		double Xj				= 0.0;
		double Xij				= 0.0;
		double total_Xkj_c1		= 0.0;
		double total_Xkj_c2		= 0.0;
		double bss				= 0.0;
		double wss				= 0.0;
		int rowlabel			= 0;
		int rowdata				= 0;


		for(int i = 0; i < nloop; i++){

			//Calculating Xkj_c1, Xkj_c2 and Xj
			//Calculating Xkj_c1 and Xj
			for(int j = 0; j < c1_length; j++){
				rowlabel = j * nloop + i;
				rowlabel = c1[rowlabel] - 1;
				rowdata = (rowlabel * COLS) + tid;
				Xkj_c1 += data[rowdata];
				Xj += data[rowdata];
			}


			//Calculating Xkj_c2 and Xj
			for(int j = 0; j < c2_length; j++){
				rowlabel = j * nloop + i;
				rowlabel = c2[rowlabel] - 1;
				rowdata = (rowlabel * COLS) + tid;
				Xkj_c2 += data[rowdata];
				Xj += data[rowdata];
			}


			//Dividing between arrays lengths.
			Xkj_c1 = (Xkj_c1 / c1_length);
			Xkj_c2 = (Xkj_c2 / c2_length);
			Xj = (Xj / c1c2_length);


			//Calculating total for Xkj_c1, Xkj_c2 and bss
			total_Xkj_c1 = ((Xkj_c1 - Xj)*(Xkj_c1 - Xj)) * c1_length;
			total_Xkj_c2 = ((Xkj_c2 - Xj)*(Xkj_c2 - Xj)) * c2_length;
			bss = (total_Xkj_c1) + (total_Xkj_c2);


			//Calculating Xij and wss
			for(int j = 0; j < c1_length; j++){
				rowlabel = j * nloop + i;
				rowlabel = c1[rowlabel] - 1;
				rowdata = (rowlabel * COLS) + tid;
				Xij = data[rowdata];
				wss += ((Xij - Xkj_c1) * (Xij - Xkj_c1));
			}

			for(int j = 0; j < c2_length; j++){
				rowlabel = j * nloop + i;
				rowlabel = c2[rowlabel] - 1;
				rowdata = (rowlabel * COLS) + tid;
				Xij = data[rowdata];
				wss += ((Xij - Xkj_c2)*(Xij - Xkj_c2));
			}

			bsswss[(i * COLS) + tid] = bss/wss;
			bss = wss = Xj = Xij = total_Xkj_c1 = total_Xkj_c2 = 0.0;
			Xkj_c1 = Xkj_c2 = 0.0;

		}//End for-loop with i


}

void cuda_bsswss(const char* ds_filename, int ds_rows, int ds_cols,
		double* ds_output, int* c1, int c1_cols, int* c2,
		int c2_cols, int nloop){

	/* Host data */
	double* ds_data = read_ds_and_fill_array(ds_filename, ds_rows, ds_cols);

	/* Device data */
	double* dev_ds_data;
	double* dev_bsswss_data;
	int* dev_c1;
	int* dev_c2;

	/* Device memory allocation */
	cudaMalloc((void**)&dev_ds_data, sizeof(double) * ds_rows * ds_cols);
	cudaMalloc((void**)&dev_bsswss_data, sizeof(double) * nloop * ds_cols);
	cudaMalloc((void**)&dev_c1, sizeof(int) * nloop * c1_cols);
	cudaMalloc((void**)&dev_c2, sizeof(int) * nloop * c2_cols);

	/* Copying data from host to device */
	cudaMemcpy(dev_ds_data, ds_data, sizeof(double) * ds_rows * ds_cols,
			cudaMemcpyHostToDevice);

	cudaMemcpy(dev_c1, c1, sizeof(int) * nloop * c1_cols,
			cudaMemcpyHostToDevice);

	cudaMemcpy(dev_c2, c2, sizeof(int) * nloop * c2_cols,
			cudaMemcpyHostToDevice);

	/* Calculating how many blocks does kernel run the threads */
	int blocks = (ds_cols + (MAX_THREADS_PER_BLOCK-1)) / MAX_THREADS_PER_BLOCK;
	int labels_length = c1_cols + c2_cols;

	/* Kernel launch */
	kernel_bsswss<<<blocks, MAX_THREADS_PER_BLOCK>>>
			(dev_ds_data, dev_c1, dev_c2, dev_bsswss_data, nloop,
					ds_rows, ds_cols, c1_cols, c2_cols, labels_length);

	/* Copying back the results */
	cudaMemcpy(ds_output, dev_bsswss_data, sizeof(double) * nloop * ds_cols,
			cudaMemcpyDeviceToHost);

	/* Freeing host and device memory */
	free(ds_data);

	cudaFree(dev_bsswss_data);
	cudaFree(dev_ds_data);
	cudaFree(dev_c1);
	cudaFree(dev_c2);

	cudaThreadExit();
}


#ifdef __cplusplus
}
#endif
